/*
 * Copyright 2020 Denis Salem
 *
 * This file is part of Tales of Lucid Dreams.
 *
 * Tales of Lucid Dreams is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Tales of Lucid Dreams is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with DStudio. If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <png.h>

typedef struct RawImage_t {
    int width;
    int height;
    unsigned char * pixel_buffer;
} RawImage;

typedef struct MetaAsset_t {
    
} MetaAsset;

int get_png_pixel(
    const char * filename,
    RawImage * raw_image
) {
    png_image image;
    memset(&image, 0, sizeof(image));
    image.version = PNG_IMAGE_VERSION;
    if (png_image_begin_read_from_file(&image, filename) != 0) {
        image.format = PNG_FORMAT_RGB;
        raw_image->width = image.width;
        raw_image->height = image.height;
        raw_image->pixel_buffer = malloc(PNG_IMAGE_SIZE(image));
        if (raw_image->pixel_buffer != NULL && png_image_finish_read(&image, NULL, raw_image->pixel_buffer, 0, NULL) != 0) {
            return PNG_IMAGE_SIZE(image);
        }
    }
    else {
        printf("Can't load asset \"%s\": %s.\n", filename, image.message);
        exit(-1);
    }
    printf("Something went wrong while reading \"%s\".\n", filename);
    exit(-1);
}

int main(int argc, char ** argv) {
    if (argc < 2) {
        printf("./asset-converter inputfile.png");
    }
    RawImage raw_image = {0};
    get_png_pixel(argv[1], &raw_image);
    
    free raw_image.pixel_buffer);
    return 0;
}
